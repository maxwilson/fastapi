from typing import Union
from pydantic import BaseModel

class UserBase(BaseModel):
	name : str
	email : str
	is_active : bool
	cep : int
	street : str | None
	neighborhood : str | None
	city : str | None
	state : str | None


class UserRequest(UserBase):
    ...

class UserResponse(UserBase):
    id: int

    class Config:
        orm_mode = True
