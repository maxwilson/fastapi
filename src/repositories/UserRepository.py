from sqlalchemy.orm import Session
from src.schemas.Users import Users

class UserRepository:
    @staticmethod
    def find_all(db: Session) -> list[Users]:
        return db.query(Users).all()

    @staticmethod
    def save(db: Session, user: Users) -> Users:
        if user.id:
            db.merge(user)
        else:
            db.add(user)
        db.commit()
        return user

    @staticmethod
    def find_by_id(db: Session, id: int) -> Users:
        return db.query(Users).filter(Users.id == id).first()

    @staticmethod
    def exists_by_id(db: Session, id: int) -> bool:
        return db.query(Users).filter(Users.id == id).first() is not None

    @staticmethod
    def delete_by_id(db: Session, id: int) -> None:
        user = db.query(Users).filter(Users.id == id).first()
        if user is not None:
            db.delete(user)
            db.commit()
