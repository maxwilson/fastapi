from sqlalchemy import Column, Integer, String, Boolean
from src.database.startConnection import Base

class Users(Base):
	
	__tablename__ = "users"

	id: int = Column(Integer, primary_key=True, index=True)
	name : str = Column(String(100), nullable=False)
	email : str = Column(String(160), nullable=False)
	is_active : bool = Column(Boolean, unique=False, default=True)
	street : str = Column(String(100), nullable=True)
	neighborhood : str = Column(String(100), nullable=True)
	city : str = Column(String(2), nullable=True)
	state : str = Column(String(60), nullable=True)
	cep : str = Column(Integer, nullable=True)
