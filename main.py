from sqlalchemy.orm import Session
from typing import Union
from fastapi import FastAPI, Depends, HTTPException, status, Response
from src.database.startConnection import engine, Base, get_db
from src.models.UserModel import UserRequest, UserResponse
from src.repositories.UserRepository import UserRepository
from src.schemas.Users import Users
from src.lib.aiohttp import session 

Base.metadata.create_all(bind=engine)

app = FastAPI()


@app.get("/api/users", response_model=list[UserResponse])
def find_all(db: Session = Depends(get_db)):
    users = UserRepository.find_all(db)
    listUsers = []
    
    if(len(users)):
        listUsers = [UserResponse.from_orm(user) for user in users]
    
    return listUsers
    
#uvicorn main:app --reload
@app.post("/api/users", response_model=UserResponse, status_code=status.HTTP_201_CREATED)
async def create(request: UserRequest, db: Session = Depends(get_db)):
    
    response = ""
    status_code = ""
    url = f"http://viacep.com.br/ws/{request.cep}/json/"

    async with session.get(url) as resp:
        status_code = resp.status
        if(resp.status != 200) :
            response = resp
        else :
            response = await resp.json()
    
    if(status_code != 200) :
        raise HTTPException(status_code=401, detail="Cep não encontrado.")

    request.street = response["logradouro"]
    request.city = response["uf"]
    request.state = response["localidade"]
    request.neighborhood = response["bairro"]

    user = UserRepository.save(db, Users(**request.dict()))
    return UserResponse.from_orm(user)


@app.get("/api/users/{id}", response_model=UserResponse)
def find_by_id(id: int, db: Session = Depends(get_db)):
    user = UserRepository.find_by_id(db, id)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Usuário não encontrado"
        )
    return UserResponse.from_orm(user)


@app.delete("/api/users/{id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_by_id(id: int, db: Session = Depends(get_db)):
    if not UserRepository.exists_by_id(db, id):
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Usuário não encontrado"
        )
    UserRepository.delete_by_id(db, id)
    return Response(status_code=status.HTTP_204_NO_CONTENT)

